from rest_framework import viewsets

from .models import Todo
from .serializers import TodoSerializer


class TodoViewSet(viewsets.ModelViewSet):
    serializer_class = TodoSerializer

    def get_queryset(self):
        if self.request.user.is_anonymous():
            return Todo.objects.none()
        return Todo.objects.filter(user=self.request.user)

