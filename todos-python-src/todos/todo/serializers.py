from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Todo

class TodoSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Todo
