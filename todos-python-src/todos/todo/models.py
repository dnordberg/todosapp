from django.db import models


class Todo(models.Model):
    "Represents a todo item"
    user = models.ForeignKey('auth.User')
    name = models.CharField(max_length=300, null=False)
    description = models.CharField(max_length=1000, blank=True)
    is_done = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("id",)

    def __unicode__(self):
        return "{} {}".format(self.user, self.name)

