
'use strict';

// Create a generic resource for each api end-point
['Todo'].forEach(function(s){
  angular.module('angularTodoApp').factory(s, ['$resource', function ($resource) {
    return $resource('/api/v1/' + camelToDash(s) + '/:id',
                     { id: '@id' },
                     {
                       put: { method: 'PUT', },
                       update: { method: 'PATCH', },
                       get: {method: 'GET'},
                       query: {isArray: true, method:'GET', params:{}},
                       delete: { method: 'DELETE' },
                       post: { method: 'POST' }
                     });
  }]);
});

