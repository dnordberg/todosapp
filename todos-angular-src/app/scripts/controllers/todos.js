'use strict';

angular.module('angularTodoApp')
.controller('TodosCtrl', function ($scope, Validate, $http, Todo) {
  $scope.newTodo = "";
  $scope.hideComplete = false;
  $scope.todoItemHasFocus = false;
  $scope.todos = [];

  $scope.removeTodo = function(index, todo){
    $scope.todos.splice(index, 1);
    Todo.delete({id: todo.id});
  }

  $scope.addTodo = function(){
    var newTodo = $scope.newTodo;
    if ($scope.newTodo){
      Todo.post({name: newTodo, description: '' , is_done: false}, function(data){
        $scope.todos.push(data.toJSON());
        $scope.newTodo = "";
      });
    }
  }

  $scope.getTodos = function(){
    Todo.query({}, function(data){
      data.forEach(function(obj){
        $scope.todos.push(obj.toJSON());
      })
    });
  };

  $scope.saveTodo = function(todo){
    if (todo.name){
      Todo.update({id: todo.id}, angular.toJson(todo), function(data){
        //pass
      });
    }
  }

  $scope.getTodos();

});
