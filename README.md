Origin Test
===========

Todo list application in django.

Requirements
------------
- User will first login to their account
- Once they login, then should see a list of their todo items.
- There is an “Add” button to add a new todo item.
- Next to each todo item, they can click “Edit”
- Next to each todo item, they can click “Mark Done”
- Next to each todo item, they can click “Delete”
- For each todo item, we store item_name, item_desc, is_done
- There is a “Hide Completed tasks” button, click on that to filter out todo
  items that are already done.

External projects in use
------------------------
- [Django API framework](http://www.django-rest-framework.org/)
- [Django Rest Auth](https://github.com/Tivix/django-rest-auth)
- [Angular js boilerplate](https://github.com/st4lk/django-rest-social-auth)

Running
-------
Install vagrant
```
vagrant up
cd /vagrant_data
docker build -t todosapp .
docker run -v /vagrant_data:/code -e SECRET_KEY=xxx -p 8000:8000 -p 22 todosapp
```

Load http://192.168.33.17:8000/ in your browser.

Development
-----------
```
docker exec {containerid} -it /bin/bash
pip install -r requirements/dev.pip
cd /code/todos-python-src
```

Tests
-----
Note: Tests are incomplete
```
docker exec {containerid} -it /bin/bash
cd /code/todos-python-src
pip install -r requirements/dev.pip
export DJANGO_SETTINGS_MODULE="todos.settings.test"
nosetests

```

