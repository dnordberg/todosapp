'use strict';

angular.module('angularTodoApp')
.controller('LogoutCtrl', function ($rootScope, $scope, $location, djangoAuth) {
  djangoAuth.logout();
  if ($rootScope.profile){
    $rootScope.profile = undefined;
    window.location.reload();
  }
});
