'use strict';


angular.module('angularTodoApp', [
               'ngCookies',
               'ngResource',
               'ngSanitize',
               'ngRoute',
               'angular-loading-bar',
])
.config(function ($routeProvider, $resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
  $routeProvider
  .when('/', {
    templateUrl: 'static/views/main.html',
    controller: 'MainCtrl',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/register', {
    templateUrl: 'static/views/register.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/passwordReset', {
    templateUrl: 'static/views/passwordreset.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/passwordResetConfirm/:firstToken/:passwordResetToken', {
    templateUrl: 'static/views/passwordresetconfirm.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/login', {
    templateUrl: 'static/views/login.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/verifyEmail/:emailVerificationToken', {
    templateUrl: 'static/views/verifyemail.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/logout', {
    templateUrl: 'static/views/logout.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/userProfile', {
    templateUrl: 'static/views/userprofile.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/passwordChange', {
    templateUrl: 'static/views/passwordchange.html',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/restricted', {
    templateUrl: 'static/views/restricted.html',
    controller: 'RestrictedCtrl',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .when('/authRequired', {
    templateUrl: 'static/views/authrequired.html',
    controller: 'AuthrequiredCtrl',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus(true);
      }],
    }
  })
  .when('/todos', {
    templateUrl: 'static/views/todos.html',
    controller: 'TodosCtrl',
    resolve: {
      authenticated: ['djangoAuth', function(djangoAuth){
        return djangoAuth.authenticationStatus();
      }],
    }
  })
  .otherwise({
    redirectTo: '/'
  });
})
.run(function(djangoAuth){
  djangoAuth.initialize('//192.168.33.17:8000/api/v1/rest-auth', false);
  //djangoAuth.initialize('http://3aca180e.eu.ngrok.io/api/v1/rest-auth', false);
});
