from .base import *

# SECRET_KEY should be passed into the docker app
SECRET_KEY = os.environ['SECRET_KEY']
DEBUG = False
