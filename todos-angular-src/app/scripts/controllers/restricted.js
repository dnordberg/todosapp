'use strict';

/**
 * @ngdoc function
 * @name angularTodoApp.controller:RestrictedCtrl
 * @description
 * # RestrictedCtrl
 * Controller of the angularTodoApp
 */
angular.module('angularTodoApp')
.controller('RestrictedCtrl', function ($scope, $location) {
  $scope.$on('djangoAuth.logged_in', function() {
    $location.path('/');
  });
});
