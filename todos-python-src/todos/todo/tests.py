# coding: utf8
from django.contrib.auth.models import User
from todos.todo.models import Todo


from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from todos.todo.models import Todo

class TodoTests(APITestCase):
    def setUp(self):
        self.maxDiff = None

        self.username_pass = {'username': "test", 'email': "test@test.com", 'pass': 'test'}

        self.user = User.objects.create(username=self.username_pass['username'],
                                        email=self.username_pass['email'])
        self.user.set_password(self.username_pass['pass'])
        self.user.save()


        self.todo1 = Todo.objects.create(user=self.user,
                                         name="lion",
                                         description="roar", is_done=True)

    def test_GET(self):
        "Test getting a list of todos"
        url = "/login/"

        response = self.client.post(url, self.username_pass, format='json')

        import ipdb; ipdb.set_trace()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Todo.objects.count(), 1)
        self.assertEqual(Todo.objects.get().name, 'DabApps')

        url = 'api/v1/rest-auth/login/'

        res = self.factory.get('api/v1/todo')

    # def test_GET_restricted(self):
    #     res = self.factory.get('api/v1/todo')
    #     self.assertRaises

    # @unittest.expectedFailure
    # def test_POST(self):
    #     self.factory.post('api/v1/rest-auth/login/', {'username': self.user.username, 'password': self.password})

    #     name = 'new todo'

    #     request = self.factory.post('/api/v1/todos', {'name': name})
    #     self.assertEqual(Todo.objects.count(), 2)

    #     todo = Todo.objects.get(pk=2)
    #     self.assertEqual(todo.name, name)

    # def test_PATCH(self):
    #     self.factory.post('api/v1/rest-auth/login/', {'username': self.user.username, 'password': self.password})

    #     name = 'test update'
    #     request = self.factory.post('/api/v1/todos/1', {'name': name})

    #     todo = Todo.objects.get(pk=1)
    #     self.assertEqual(todo.name, name)

    # def test_DELETE(self):
    #     self.factory.post('api/v1/rest-auth/login/', {'username': self.user.username, 'password': self.password})

    #     request = self.factory.delete('/api/v1/todos/1')

    #     self.assertEqual(Todo.objects.count(), 0)

