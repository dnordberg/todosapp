FROM ubuntu:trusty
MAINTAINER Daniel Nordberg <dnordberg@gmail.com>

# Add repos, software-properties-common is required for add-apt-repository
RUN apt-get update && \
   DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common && \
   DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:chris-lea/node.js

# lib32ncurses5-dev is for readline support useful for ipython debugging
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -yq install \
  locales \
  nodejs \
  nginx \
  openssh-server \
  supervisor \
  python-software-properties \
  git \
  build-essential \
  python-pip python-dev lib32ncurses5-dev

RUN dpkg-reconfigure locales && \
    locale-gen en_US.UTF-8 && \
    /usr/sbin/update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8

RUN mkdir -p /var/run/sshd

RUN echo "root:root" | chpasswd

COPY ./todos-python-src/requirements /tmp/
RUN pip install -r /tmp/prod.pip

RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN rm -f /code/todos-python-src/todos/db.sqlite3

# setup all the configfiles
run echo "daemon off;" >> /etc/nginx/nginx.conf
run rm /etc/nginx/sites-enabled/default
run ln -s /code/deploy/nginx-app.conf /etc/nginx/sites-enabled/
run ln -s /code/deploy/supervisor-app.conf /etc/supervisor/conf.d/
RUN ln -s /code/sshd.conf /etc/supervisor/conf.d/

RUN cd todos-angular-src && npm install -g \
    bower \
    grunt-cli

RUN cd todos-angular-src && npm install && bower install --allow-root && grunt build

EXPOSE 22 80

VOLUME ["/code"]

CMD ["/bin/bash", "/code/todos-python-src/start.sh"]


